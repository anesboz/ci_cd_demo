#!/bin/bash
set -e

private_key_path="~/private_key"
cat $PRIVATE_KEY > $private_key_path
# chmod 600 ~/.ssh/id_rsa

ssh -i $private_key_path $HOST_ADDRESS 'bash -s' < ./deploy/updateAndRestart.sh
